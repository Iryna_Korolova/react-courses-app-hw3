import { LOGIN, LOGOUT } from './actionTypes';

export function logIn(userData) {
	return {
		type: LOGIN,
		payload: userData,
	};
}
export function logOut() {
	return {
		type: LOGOUT,
	};
}
