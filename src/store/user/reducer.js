import { LOGIN, LOGOUT } from './actionTypes';

const initialState = {
	isAuth: false,
	name: '',
	email: '',
	token: '',
};

export function userReducer(state = initialState, action) {
	switch (action.type) {
		case LOGIN:
			return { isAuth: true, ...action.payload };
		case LOGOUT:
			return { isAuth: false, name: '', email: '', token: '' };
		default:
			return state;
	}
}
