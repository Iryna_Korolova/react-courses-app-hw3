export const SET_ALL_COURSES = 'SET_ALL_COURSES';
export const ADD_COURSE = 'ADD_COURSE';
export const DELETE_COURSE = 'DELETE_COURSE';
