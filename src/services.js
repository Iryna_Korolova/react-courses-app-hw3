import axios from 'axios';

const api = axios.create({
	baseURL: 'http://localhost:3000/',
});

api.interceptors.response.use(
	(response) => Promise.resolve(response.data),
	(error) => Promise.reject(error)
);

export function getCourses() {
	return api.get(`/courses/all`);
}

export function getAllAuthors() {
	return api.get(`/authors/all`);
}

export function logInUser(loginData) {
	return api.post(`/login`, loginData);
}
