import { useState, useEffect } from 'react';

import CourseCard from './CourseCard/CourseCard';
import SearchBar from './components/SearchBar/SearchBar';
import Button from '../../common/Button/Button';
import CreateCourse from '../CreateCourse/CreateCourse';
import CourseInfo from '../CourseInfo/CourseInfo';

import { useNavigate, Route, Routes } from 'react-router-dom';

import { getAllAuthors, getCourses } from '../../services';
import { useDispatch, useSelector } from 'react-redux';
import { setAllCourses } from '../../store/courses/actionCreators';
import { coursesSelector } from '../../store/courses/selectors';
import { setAllAuthors } from '../../store/authors/actionCreators';

export default function Courses() {
	const courses = useSelector(coursesSelector);
	const dispatch = useDispatch();
	const navigate = useNavigate();
	const [searchValue, setSearchValue] = useState('');

	useEffect(() => {
		getAllAuthors()
			.then((authors) => dispatch(setAllAuthors(authors.result)))
			.catch((err) => {
				console.log(err);
			});
		getCourses()
			.then((courses) => dispatch(setAllCourses(courses.result)))
			.catch((err) => {
				console.log(err);
			});
	}, []);

	return (
		<main>
			<Routes>
				<Route
					path=''
					element={
						<div className='container'>
							<div className='searchbar-wrap'>
								<SearchBar setSearchValue={setSearchValue}></SearchBar>
								<Button
									buttonText='Add new course'
									onClick={() => navigate('/courses/add')}
								></Button>
							</div>
							{courses
								.filter((course) =>
									`${course.title} ${course.id}`
										.toLocaleLowerCase()
										.includes(searchValue)
								)
								.map((course) => (
									<CourseCard key={course.id} course={course}></CourseCard>
								))}
						</div>
					}
				></Route>
				<Route path='add' element={<CreateCourse />}></Route>
				<Route
					path=':courseId'
					element={<CourseInfo courses={courses}></CourseInfo>}
				></Route>
			</Routes>
		</main>
	);
}
