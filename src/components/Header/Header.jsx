import Logo from './components/Logo/Logo';
import Button from '../../common/Button/Button';
import './Header.css';

import { useNavigate, useLocation } from 'react-router-dom';
import { useEffect } from 'react';

import { useDispatch, useSelector } from 'react-redux';
import { logOut, logIn } from '../../store/user/actionCreators';
import { userSelector } from '../../store/user/selectors';

export default function Header() {
	const dispatch = useDispatch();
	const user = useSelector(userSelector);
	const navigate = useNavigate();
	const location = useLocation();

	useEffect(() => {
		const savedToken = window.localStorage.getItem('token');
		if (savedToken) {
			const savedUserData = JSON.parse(window.localStorage.getItem('user'));
			dispatch(logIn({ token: savedToken, ...savedUserData }));
		}
	}, []);

	useEffect(() => {
		const signRoutes = ['/login', '/registration'];
		if (
			signRoutes.some((path) => location.pathname.startsWith(path)) &&
			user.isAuth
		) {
			navigate('/courses', { replace: true });
		}
		if (
			!signRoutes.some((path) => location.pathname.startsWith(path)) &&
			!user.isAuth
		) {
			navigate('/login', { replace: true });
		}
	}, [location, navigate, user]);

	function signOut() {
		window.localStorage.removeItem('token');
		window.localStorage.removeItem('user');
		dispatch(logOut());
		navigate('/login', { replace: true });
	}
	return (
		<header className='header container'>
			<div className='header-inner'>
				<Logo />
				{user.isAuth && (
					<>
						<div className='header-inner'>
							<h3 className='header-heading'>{user.name}</h3>
							<Button buttonText='Logout' onClick={signOut} />
						</div>
					</>
				)}
			</div>
		</header>
	);
}
