import Button from '../../common/Button/Button';
import Input from '../../common/Input/Input';

import { useState, useEffect } from 'react';

import { pipeDuration } from '../../helpers/pipeDuration';

import './CreateCourse.css';

import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { addNewCourse } from '../../store/courses/actionCreators';
import { authorsSelector } from '../../store/authors/selectors';
import { addAuthor } from '../../store/authors/actionCreators';

export default function CreateCourse() {
	const dispatch = useDispatch();
	const navigate = useNavigate();
	const storedAuthorsList = useSelector(authorsSelector);
	const [authorsList, setAuthorsList] = useState([]);
	const [courseAuthorsList, setCourseAuthorsList] = useState([]);
	const [duration, setDuration] = useState(0);
	const [formattedDuration, setFormattedDuration] = useState('');
	const [errorMessage, setErrorMessage] = useState('');

	useEffect(() => {
		const checkedAuthorsList = storedAuthorsList.reduce((list, author) => {
			const isExist = courseAuthorsList.find(
				(courseAuthor) => courseAuthor.id === author.id
			);
			if (!isExist) {
				return [...list, author];
			} else {
				return list;
			}
		}, []);
		setAuthorsList(checkedAuthorsList);
	}, [storedAuthorsList]);

	useEffect(() => {
		setFormattedDuration(pipeDuration(duration));
	}, [duration]);

	function onSubmit(event) {
		event.preventDefault();
		if (courseAuthorsList.length < 1) {
			return setErrorMessage('Please add author to the course');
		}
		const newCourse = {
			id: String(Date.now()),
			title: event.target.title.value.trim(),
			description: event.target.description.value.trim(),
			creationDate: new Date().toISOString(),
			duration: duration,
			authors: courseAuthorsList.map((author) => author.id),
		};
		dispatch(addNewCourse(newCourse));
		navigate('/courses');
	}
	function changeDuration(event) {
		setDuration(Number(event.target.value));
	}
	function addAuthorToCourse(authorId) {
		const author = authorsList.find((author) => author.id === authorId);
		setCourseAuthorsList([...courseAuthorsList, author]);
		const newAuthorsList = authorsList.filter(
			(author) => author.id !== authorId
		);
		setAuthorsList(newAuthorsList);
	}
	function deleteAuthor(authorId) {
		const author = courseAuthorsList.find((author) => author.id === authorId);
		setAuthorsList([...authorsList, author]);
		const newCourseAuthorsList = courseAuthorsList.filter(
			(author) => author.id !== authorId
		);
		setCourseAuthorsList(newCourseAuthorsList);
	}
	function addNewAuthor(event) {
		event.preventDefault();
		const newAuthor = {
			id: String(Date.now()),
			name: event.target.authorName.value.trim(),
		};
		dispatch(addAuthor(newAuthor));
		event.target.reset();
	}

	return (
		<div className='container create-wrap'>
			<form className='create-section-wrap' onSubmit={onSubmit}>
				<div className='form-wrap'>
					<div className='form-input'>
						<Input
							labelText='Title'
							placeholdetText='Enter title...'
							inputName='title'
							required
						></Input>
					</div>
					<div className='form-btn'>
						<Button buttonText='Create course' buttonType='submit'></Button>
					</div>
				</div>
				<label className='textarea-label' htmlFor='description'>
					Description
				</label>
				<textarea
					className='textarea'
					placeholder='Enter description'
					id='description'
					name='description'
					minLength='2'
					rows='4'
					cols='50'
					required
				></textarea>
			</form>
			<div className='author-wrap'>
				<div className='add-author-section'>
					<div className='message'>{errorMessage && <p>{errorMessage}</p>}</div>
					<form className='author-form' onSubmit={addNewAuthor}>
						<h3 className='author-name-title'>Add author</h3>
						<Input
							labelText='Author name'
							placeholdetText='Enter author name...'
							inputName='authorName'
							required
						></Input>
						<div className='form-wrap-btn'>
							<Button buttonText='Create author' buttonType='submit'></Button>
						</div>
						<h3 className='author-title'>Duration</h3>
						<Input
							labelText='Duration'
							placeholdetText='Enter duration in minutes...'
							inputType='number'
							inputName='duration'
							onChange={changeDuration}
						></Input>
						<p className='duration'>Duration: {formattedDuration}</p>
					</form>
				</div>
				<div className='authors-section'>
					<h3 className='authors-section-title'>Authors</h3>
					{authorsList.map((author) => (
						<div key={author.id} className='authors-wrap'>
							<h4>{author.name}</h4>
							<Button
								buttonText='Add author'
								onClick={() => addAuthorToCourse(author.id)}
							></Button>
						</div>
					))}
					<h4 className='authors-section-title'>Course authors</h4>
					{courseAuthorsList.length < 1 ? (
						<h5>Author list is empty</h5>
					) : (
						courseAuthorsList.map((author) => (
							<div key={author.id} className='authors-wrap'>
								<h4>{author.name}</h4>
								<Button
									buttonText='Delete author'
									onClick={() => deleteAuthor(author.id)}
								></Button>
							</div>
						))
					)}
				</div>
			</div>
		</div>
	);
}
