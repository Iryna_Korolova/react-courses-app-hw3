import { mockedAuthorsList } from '../../constants';
import { dateGenerator } from '../../helpers/dateGenerator';
import { pipeDuration } from '../../helpers/pipeDuration';

import { Link, useParams } from 'react-router-dom';

import './CourseInfo.css';

export default function CourseInfo({ courses }) {
	const { courseId } = useParams();
	const courseData = courses.find((course) => course.id === courseId);
	if (!courseData) {
		return <h1>No course found</h1>;
	}
	const authorsNames = courseData.authors.map((authorId) => (
		<p key={authorId}>
			{mockedAuthorsList.find((author) => author.id === authorId)?.name}
		</p>
	));
	return (
		<div className='container info-container'>
			<Link to='/courses'>&#8249; Back to courses</Link>
			<>
				<h2 className='info-title'>{courseData.title}</h2>
				<div className='info-wrap'>
					<div className='info-description-wrap'>
						<p>{courseData.description}</p>
					</div>
					<div className='info-authors-wrap'>
						<h3 className='info-subtitle'>ID: {courseId}</h3>
						<h3 className='info-subtitle'>
							Duration: {pipeDuration(courseData.duration)}
						</h3>
						<h3 className='info-subtitle'>
							Created: {dateGenerator(courseData.creationDate)}
						</h3>
						<h3>
							Authors:
							{authorsNames}
						</h3>
					</div>
				</div>
			</>
		</div>
	);
}
